﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.Interface.DataModels
{
   public interface IDataComment
    {
         int IdComment { get;  }
         int Id_task { get;  }
         string Comment1 { get;  }
         DateTime Date_creation { get;  }
    }
}
