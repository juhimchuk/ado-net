﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.Interface.DataModels
{
    public interface IDataUser
    {
         int IdUser { get;  }
         string Name { get;  }
         string Password { get;  }
    }
}
