﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.Interface.DataModels
{
   public interface IDataTask
    {
         int IdTask { get; }
         int Id_todolist { get;  }
         string Name { get;  }
         string Description { get;  }
         int Priority { get;  }
         string Status { get;  }
         DateTime Date_creation { get;  }
         DateTime Date_finish { get;  }
         int Reminders { get;  }
    }
}
