﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.Interface.DataModels
{
    public interface IDataToDoList
    {
         int IdToDoList { get;  }
         int Id_category { get;  }
         string Name { get;  }
         int Priority { get;  }
         string Description { get;  }
    }
}
