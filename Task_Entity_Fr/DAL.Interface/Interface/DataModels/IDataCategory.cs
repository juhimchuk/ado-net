﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.Interface.DataModels
{
   public interface IDataCategory
    {
         int IdCategory { get;  }
         int Id_user { get;  }
         string Name { get;  }
    }
}
