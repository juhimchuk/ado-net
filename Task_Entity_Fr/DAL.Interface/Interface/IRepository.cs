﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interface.Interface
{
    public interface IRepository<TEntity, TDto>
    {
        bool Create(TDto model);
        bool Create(IEnumerable<TDto> model);
        void Update(TDto model);
        void Delete(int id);
        IEnumerable<TDto> Get(Func<TEntity, bool> predicate);
        int Gets(Func<TEntity, bool> predicate);
    }
}
