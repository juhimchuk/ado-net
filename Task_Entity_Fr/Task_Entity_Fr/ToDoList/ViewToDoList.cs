﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.Category;


namespace Task_Entity_Fr.ToDoList
{
    public class ViewToDoList
    {
         public void View_todolist(int idcat, int iduser)
        {
            var viewModel = new ToDoListViewModel
            {
                Id_category = idcat
            };

            var service = new ToDoListServices();
            var cat = service.Get(viewModel);
            Console.WriteLine("ToDoList:");
            foreach (var dir in cat)
            {
                Console.WriteLine(dir.Name+" "+dir.Priority );
            }
            Console.WriteLine();
            
            var update = new UpdateCategory();
            update.Update_cat(iduser);
        }
    }
}
