﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.Category;

namespace Task_Entity_Fr.ToDoList
{
   public class DeleteToDoList
    {
        public void Deletetodolist(int iduser, int idcat)
        {
            int count = 0;
            do
            {
               
                var viewModel = new ToDoListViewModel
                {
                    Id_category = idcat
                };
                var service = new ToDoListServices();
                var cat = service.Get(viewModel);
                Console.Write("Enter todolist name - ");
                string todo_name = Console.ReadLine();
                foreach (var dir in cat)
                {
                    if (todo_name == dir.Name.Trim())
                    {
                        service.Delete(dir.IdToDoList);
                        count++;
                    }
                }
            }
            while (count == 0);

            Console.WriteLine();
            var update = new UpdateCategory();
            update.Update_cat(iduser);
        }
    }
}
