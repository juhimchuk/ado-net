﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.Category;

namespace Task_Entity_Fr.ToDoList
{
    public class AddToDoList
    {
        public void Add_todolist(int idcat, int iduser)
        {
            string todo_name, descript, priority,save="";
            bool control;
            do
            {
                Console.Write("Enter todolist name - ");
                todo_name = Console.ReadLine();
                control = String.IsNullOrEmpty(todo_name);
            }
            while (control);
            do
            {
                Console.Write("Enter description - ");
                descript = Console.ReadLine();
                control = String.IsNullOrEmpty(descript);
            }
            while (control);
            do
            {
                Console.Write("Enter priority - ");
                priority = Console.ReadLine();
                int number;
                control = Int32.TryParse(priority, out number);
            }
            while (!control);
            int prior = Convert.ToInt32(priority);
           

            do
            {

                var viewModel = new ToDoListViewModel
                {
                    Id_category = idcat,
                    Name = todo_name,
                    Priority = prior,
                    Description = descript
                };
                var service = new ToDoListServices();
                var cat = service.Get(viewModel);
                int count = 0;
                foreach (var dir in cat)
                {
                    if (todo_name == dir.Name.Trim() && idcat == dir.Id_category)
                    {
                        count++;
                    }
                }
                if (count == 0)
                {
                    service.Create(viewModel);
                    save = "ok";
                }
                else
                {
                    Console.WriteLine("Enter another todolist name ; ");
                    save = "";
                }
                Console.WriteLine();
            }
            while (save == "");
            var update = new UpdateCategory();
            update.Update_cat(iduser);
        }
    }
}
