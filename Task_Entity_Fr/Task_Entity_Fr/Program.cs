﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;

namespace Task_Entity_Fr
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice;
            do
            {
                Console.WriteLine("Enter login for enter or logup for registration or exit for exit");
                Console.Write("Enter action - ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "login":
                        var login = new User_login();
                        login.Log_in();
                        break;
                    case "logup":
                        var logup = new User_logup();
                        logup.Log_up();
                        break;
                }
            }
            while (choice != "exit");
        
        }
    }
}
