﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using System.Threading.Tasks;

namespace Task_Entity_Fr.Category
{
    public class Viewcategory
    {
        public void View_cat(int iduser)
        {
            var viewModel = new CategoryViewModel
            {
                Id_user = iduser
            };

            var service = new CategoryServices();
            var cat = service.Get(viewModel);
            Console.WriteLine("Category:");
            foreach (var dir in cat)
            {
                Console.WriteLine(dir.Name);
            }
            Console.WriteLine();
            var choice = new Choice_action();
            choice.Choice(iduser);
        }
    }
}
