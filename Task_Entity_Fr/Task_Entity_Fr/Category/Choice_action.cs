﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Entity_Fr.Category
{
   public class Choice_action
    {
        public void Choice(int iduser)
        {
            Console.WriteLine("Choice of action.");
            Console.WriteLine(@"1 - View all category.
2 - Add new category.
3 - Delete category.
4 - Update category.
5 - Change name category.
back - back to login.");

            string choice;
            do
            {
                Console.Write("Enter action - ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        var view = new Viewcategory();
                       view.View_cat(iduser);
                        break;
                    case "2":
                        var add = new Add_category();
                        add.Add_cat(iduser);
                        break;
                    case "3":
                        var del = new DeleteCategory();
                        del.DeleteCat(iduser);
                        break;
                    case "4":
                        var update = new UpdateCategory();
                        update.Update_cat(iduser);
                        break;
                    case "5":
                        var change = new Change_name_category();
                        change.Change(iduser);
                        break;
                    case "back":
                        var back = new User_login();
                        back.Log_in();
                        break;
                    default:
                        Console.WriteLine("Select the correct action.");
                        choice = "repeat";
                        break;
                }
            }
            while (choice == "repeat");
        }
    }
}
