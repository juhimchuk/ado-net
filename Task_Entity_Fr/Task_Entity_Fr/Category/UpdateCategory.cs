﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.ToDoList;

namespace Task_Entity_Fr.Category
{
    public class UpdateCategory
    {
        public void Update_cat(int iduser)
        {
            var update = new UpdateCategory();
            int idcat;
            Console.WriteLine("Choice of action.");
            Console.WriteLine(@"1. View all todolist.
2. Add new todolist.
3. Delete todolist.
back.");
            string choice = Console.ReadLine();
            do
            {
                switch (choice)
                {
                    case "1":
                        idcat = update.Choice_cat(iduser);
                        var view = new ViewToDoList();
                       view.View_todolist( idcat, iduser);
                        break;
                    case "2":
                        idcat = update.Choice_cat(iduser);
                        var add = new AddToDoList();
                         add.Add_todolist(idcat, iduser);
                        break;
                    case "3":
                        idcat = update.Choice_cat(iduser);
                        var delete = new DeleteToDoList();
                        delete.Deletetodolist(iduser, idcat);
                        break;
                    case "back":
                        var choic = new Choice_action();
                        choic.Choice(iduser);
                        break;
                    default:
                        Console.WriteLine("Select the correct action.");
                        choice = "repeat";
                        break;
                }
            }
            while (choice == "repeat");
        }
        public int Choice_cat(int iduser)
        {
            int count = 0;
            int idcat = -1;
            string cat_name;
            do
            {
                Console.WriteLine();
                Console.Write("Enter category name - ");
                cat_name = Console.ReadLine();
                var viewModel = new CategoryViewModel
                {
                    Id_user = iduser
                };
                
                var service = new CategoryServices();
                var cat = service.Get(viewModel);
                foreach (var dir in cat)
                {
                    if (cat_name == dir.Name.Trim())
                    {
                        idcat = dir.IdCategory;
                        count++;
                    }
                }
               
                if (count == 0)
                {

                    Console.WriteLine("Enter the name of the category that exists.");
                }
            }
            while (count == 0);
            Console.WriteLine();

            return idcat;
        }
    }
}
