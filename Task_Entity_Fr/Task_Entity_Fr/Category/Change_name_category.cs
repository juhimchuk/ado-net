﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.Category;

namespace Task_Entity_Fr.Category
{
   public class Change_name_category
    {
        public void Change(int iduser)
        {
            int count = 0;

            var viewModel = new CategoryViewModel
            {
                Id_user=iduser
            };

            var service = new CategoryServices();
            var cat=service.Get(viewModel);
            string cat_name;
            do
            {
                int idcategory = -1;
                Console.WriteLine();
                Console.Write("Enter category name - ");
                cat_name = Console.ReadLine();
                Console.Write("Enter new category name - ");
                string new_cat_name = Console.ReadLine();
                foreach (var dir in cat)
                {
                    if (cat_name == dir.Name.Trim())
                    {
                        idcategory = dir.IdCategory;
                        count++;
                    }
                }
                if (count != 0)
                {
                    if (cat_name != new_cat_name)
                    {
                         viewModel = new CategoryViewModel
                        {
                            Id_user = iduser,
                            IdCategory=idcategory,
                            Name=new_cat_name
                        };
                        service.Update(viewModel);
                        cat_name = "repeat";
                    }
                    else
                    {
                        Console.WriteLine("Enter a name for the category other than the existing one.");
                    }
                }
                else
                {
                    Console.WriteLine("Enter the name of the category that exists.");
                }

            }
            while (cat_name != "repeat");
            Console.WriteLine();
            var choice = new Choice_action();
            choice.Choice(iduser);
        }
    }
}
