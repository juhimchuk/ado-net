﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;

namespace Task_Entity_Fr.Category
{
    public class Add_category
    {
        public void Add_cat(int iduser)
        {
            bool control;
            string cat_name;
            do
            {
                Console.Write("Enter category name - ");
                cat_name = Console.ReadLine();
                control = String.IsNullOrEmpty(cat_name);
            }
            while (control);
            var viewModel = new CategoryViewModel
            {
                Name = cat_name,
              Id_user = iduser
            };
            var service = new CategoryServices();
            var cat = service.Get(viewModel);
            int count = 0;
            foreach (var dir in cat)
            {
                if (cat_name == dir.Name.Trim() && iduser==dir.Id_user)
                {
                    count++;
                }
            }
            if (count == 0)
            {
                service.Create(viewModel);
            }
            else
            {
                Console.WriteLine("Enter another category name ; ");
            }
            Console.WriteLine();
            var choice = new Choice_action();
            choice.Choice(iduser);
        }
    }
}
