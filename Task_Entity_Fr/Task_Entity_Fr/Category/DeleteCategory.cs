﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;

namespace Task_Entity_Fr.Category
{
    public class DeleteCategory
    {
        public void DeleteCat(int iduser)
        {
            int count = 0;
            do
            {
                var viewModel = new CategoryViewModel
                {
                    Id_user = iduser
                };
                int idcategory=-1;
                var service = new CategoryServices();
                var cat = service.Get(viewModel);
                Console.Write("Enter category name - ");
                string cat_name = Console.ReadLine();
                foreach (var dir in cat)
                {
                    if (cat_name == dir.Name.Trim())
                    {
                        service.Delete(dir.IdCategory);
                        count++;
                    }
                }
            }
            while (count == 0);
            Console.WriteLine();
            var choice = new Choice_action();
            choice.Choice(iduser);
        }
    }
}
