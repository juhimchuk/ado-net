﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interface.Interface.DataModels;

namespace Task_Entity_Fr.ViewModel
{
	public class CategoryViewModel:IDataCategory
	{
		public int IdCategory { get; set; }
		public int Id_user { get; set; }
		public string Name { get; set; }
		public string UserName { get; set; }
		public List<string> TaskName { get; set; }
		public List<int> Priority { get; set; }
	}
}
