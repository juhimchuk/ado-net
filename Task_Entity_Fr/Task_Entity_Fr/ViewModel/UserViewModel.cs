﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interface.Interface.DataModels;

namespace Task_Entity_Fr.ViewModel
{
	public class UserViewModel:IDataUser
	{
		public int IdUser { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
		public List<string> CategoryName { get; set; }
	}
}
