﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.DTO
{
    public class CategoryDto:IDataCategory
    {
        public int IdCategory { get; set; }
        public int Id_user { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public List<string> TaskName { get; set; }
        public List<int> Priority { get; set; }

    }
}
