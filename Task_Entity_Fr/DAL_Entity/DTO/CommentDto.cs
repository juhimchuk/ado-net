﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.DTO
{
    public class CommentDto:IDataComment
    {
        public int IdComment { get; set; }
        public int Id_task { get; set; }
        public string Comment1 { get; set; }
        public System.DateTime Date_creation { get; set; }
        public string TaskName { get; set; }
    }
}
