﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class UserRepository : IRepository<User, IDataUser>
    {
        private User ToEntity(IDataUser model)
        {
            return new User
            {
                Name = model.Name,
                Password = model.Password,
              
            };
        }
        public bool Create(IDataUser model)
        {
            var user = ToEntity(model);
			bool result = false;
			using (var context = new ToDoListEntities1())
            {
                context.User.Add(user);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataUser> model)
        {
            var userEntities = model
                            .Select(ToEntity);

            bool result = false;

            using (var context = new ToDoListEntities1())
            {
                context.User.AddRange(userEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities1())
            {
                var user = context.User
                    .SingleOrDefault(o => o.IdUser.Equals(id));

                if (user != null)
                {
                    context.User.Remove(user);
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IDataUser> Get(Func<User, bool> predicate)
        {
            var result = new List<IDataUser>();

            using (var context = new ToDoListEntities1())
            {
                result = context.User
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }
        public int Gets(Func<User, bool> predicate)
        {

            using (var context = new ToDoListEntities1())
            {
                var result = context.User
                    .SingleOrDefault(predicate);
                if (result!=null)
                return result.IdUser;
            }

            return -1;
        }
        private IDataUser EntityToDto(User model)
        {
            return new UserDto
            {
                IdUser=model.IdUser,
                Name=model.Name,
                Password=model.Password
            };
        }
        public void Update(IDataUser model)
        {
            using (var context = new ToDoListEntities1())
            {
                var modelToUpdate = context.User
                    .SingleOrDefault(o => model.IdUser.Equals(o.IdUser));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Password = model.Password;
                }

                context.SaveChanges();
            }

        }
        public bool Any(Func<User, bool> predicate)
        {
            bool result = false;

            using (var context = new ToDoListEntities1())
            {
                result = context.User.Any(predicate);
            }

            return result;
        }
    }
}
