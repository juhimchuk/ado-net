﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class CategoryRepository : IRepository<Category, IDataCategory>
    {
        private Category DtoToEntity(IDataCategory model)
        {
			return new Category
			{
				
                Name = model.Name,
                Id_user = model.Id_user               
            };
        }
        public bool Create(IDataCategory model)
        {
            var category = DtoToEntity(model);
			bool result = false;

			using (var context = new ToDoListEntities1())
            {
                context.Category.Add(category);

				result = context.SaveChanges() > 0;
			}

            return result;
        }

        public bool Create(IEnumerable<IDataCategory> model)
        {
            var categoryEntities = model.Select(DtoToEntity);

            bool result = false;

            using (var context = new ToDoListEntities1())
            {
                context.Category.AddRange(categoryEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities1())
            {
                var category = context.Category
                    .SingleOrDefault(o => o.IdCategory.Equals(id));

                if (category != null)
                {
                    context.Category.Remove(category);
                }

                context.SaveChanges();
            }
        }
        public int Gets(Func<Category, bool> predicate)
        {


            return -1;
        }
        public IEnumerable<IDataCategory> Get(Func<Category, bool> predicate)
        {
            var result = new List<IDataCategory>();

            using (var context = new ToDoListEntities1())
            {
                result = context.Category
                    .Include(nameof(User))
                    .Include(nameof(ToDoList))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }
        private IDataCategory EntityToDto(Category model)
        {
            return new CategoryDto
            {
                Name = model.Name,
                IdCategory = model.IdCategory,
                Id_user = model.Id_user,
                UserName = model.User?.Name,
                TaskName = model.ToDoList?.Select(o => o.Name).ToList(),
                Priority = model.ToDoList?.Select(o => o.Priority).ToList()
            };
        }
        public void Update(IDataCategory model)
        {
            using (var context = new ToDoListEntities1())
            {
                var modelToUpdate = context.Category
                    .SingleOrDefault(o => model.IdCategory.Equals(o.IdCategory));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Id_user = model.Id_user;
                }

                context.SaveChanges();
            }
        }
		public bool Any(Func<Category, bool> predicate)
		{
			bool result = false;

			using (var context = new ToDoListEntities1())
			{
				result = context.Category.Any(predicate);
			}

			return result;
		}
	}
}
