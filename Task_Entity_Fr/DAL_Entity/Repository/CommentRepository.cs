﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class CommentRepository : IRepository<Comment, IDataComment>
    {
        private Comment DtoToEntity(IDataComment model)
        {
            return new Comment
            {
                Id_task=model.Id_task,
                Comment1=model.Comment1,
                Date_creation=model.Date_creation
            };
        }
        public bool Create(IDataComment model)
        {
            var comment = DtoToEntity(model);
			bool result = false;
			using (var context = new ToDoListEntities1())
            {
                context.Comment.Add(comment);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataComment> model)
        {
            var commentEntities = model.Select(DtoToEntity);

            bool result = false;

            using (var context = new ToDoListEntities1())
            {
                context.Comment.AddRange(commentEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities1())
            {
                var comment = context.Comment
                    .SingleOrDefault(o => o.IdComment.Equals(id));

                if (comment != null)
                {
                    context.Comment.Remove(comment);
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IDataComment> Get(Func<Comment, bool> predicate)
        {
            var result = new List<IDataComment>();

            using (var context = new ToDoListEntities1())
            {
                result = context.Comment
                    .Include(nameof(Model.Task))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }
        private IDataComment EntityToDto(Comment model)
        {
            return new CommentDto
            {
                IdComment= model.IdComment,
                Id_task = model.Id_task,
                Date_creation = model.Date_creation,
                Comment1 = model.Comment1,
                TaskName=model.Task?.Name
            };
        }

        public void Update(IDataComment model)
        {
            using (var context = new ToDoListEntities1())
            {
                var modelToUpdate = context.Comment
                    .SingleOrDefault(o => model.IdComment.Equals(o.IdComment));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Id_task = model.Id_task;
                    modelToUpdate.Comment1 = model.Comment1;
               
                }

                context.SaveChanges();
            }
        }
        public int Gets(Func<Comment, bool> predicate)
        {


            return -1;
        }
        public bool Any(Func<Comment, bool> predicate)
		{
			bool result = false;

			using (var context = new ToDoListEntities1())
			{
				result = context.Comment.Any(predicate);
			}

			return result;
		}
	}
}
