﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class TaskRepository : IRepository<Model.Task, IDataTask>
    {
        private Model.Task DtoToEntity(IDataTask model)
        {
            return new Model.Task
            {
                Id_todolist=model.Id_todolist,
                Name = model.Name,
                Description=model.Description,
                Priority = model.Priority,
                Status=model.Status,
                Date_creation=model.Date_creation,
                Date_finish=model.Date_finish,
                Reminders=model.Reminders
            };
        }
        public bool Create(IDataTask model)
        {
            var task = DtoToEntity(model);
			bool result = false;
			using (var context = new ToDoListEntities1())
            {
                context.Task.Add(task);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataTask> model)
        {
            var taskEntities = model.Select(DtoToEntity);

            bool result = false;

            using (var context = new ToDoListEntities1())
            {
                context.Task.AddRange(taskEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities1())
            {
                var task = context.Task
                    .SingleOrDefault(o => o.IdTask.Equals(id));

                if (task != null)
                {
                    context.Task.Remove(task);
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IDataTask> Get(Func<Model.Task, bool> predicate)
        {
            var result = new List<IDataTask>();

            using (var context = new ToDoListEntities1())
            {
                result = context.Task
                    .Include(nameof(ToDoList))
                    .Include(nameof(ICollection<Comment>))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }
        private IDataTask EntityToDto(Model.Task model)
        {
            return new TaskDto
            {
                IdTask = model.IdTask,
                Id_todolist = model.Id_todolist,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                Status = model.Status,
                Date_creation = model.Date_creation,
                Date_finish = model.Date_finish,
                Reminders = model.Reminders,
                ListName = model.ToDoList?.Name,
                Comment = model.Comment?.Select(o => o.Comment1).ToList()
            };
        }
        public int Gets(Func<Model.Task, bool> predicate)
        {


            return -1;
        }
        public void Update(IDataTask model)
        {
            using (var context = new ToDoListEntities1())
            {
                var modelToUpdate = context.Task
                    .SingleOrDefault(o => model.IdTask.Equals(o.IdTask));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Id_todolist = model.Id_todolist;
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Description = model.Description;
                    modelToUpdate.Priority = model.Priority;
                    modelToUpdate.Status = model.Status;
                    modelToUpdate.Date_finish = model.Date_finish;
                    modelToUpdate.Reminders = model.Reminders;
                }

                context.SaveChanges();
            }

        }
		public bool Any(Func<Model.Task, bool> predicate)
		{
			bool result = false;

			using (var context = new ToDoListEntities1())
			{
				result = context.Task.Any(predicate);
			}

			return result;
		}
	}
}
