﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class ToDoListRepository : IRepository<ToDoList, IDataToDoList>
    {
        private ToDoList DtoToEntity(IDataToDoList model)
        {
            return new ToDoList
            {
                Id_category = model.Id_category,
                Name = model.Name,
                Priority = model.Priority,
                Description = model.Description
            };
        }
        public bool Create(IDataToDoList model)
        {
            var todolist = DtoToEntity(model);
			bool result = false;
			using (var context = new ToDoListEntities1())
            {
                context.ToDoList.Add(todolist);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataToDoList> model)
        {
            var todolistEntities = model.Select(DtoToEntity);

            bool result = false;

            using (var context = new ToDoListEntities1())
            {
                context.ToDoList.AddRange(todolistEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities1())
            {
                var todolist = context.ToDoList
                    .SingleOrDefault(o => o.IdToDoList.Equals(id));

                if (todolist != null)
                {
                    context.ToDoList.Remove(todolist);
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IDataToDoList> Get(Func<ToDoList, bool> predicate)
        {
            var result = new List<IDataToDoList>();

            using (var context = new ToDoListEntities1())
            {
                result = context.ToDoList
                    .Include(nameof(Category))
                    .Include(nameof(Model.Task))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).ToList();
            }

            return result;
        }
        public int Gets(Func<ToDoList, bool> predicate)
        {
            

            return -1;
        }
        private IDataToDoList EntityToDto(ToDoList model)
        {
            return new ToDoListDto
            {
                IdToDoList = model.IdToDoList,
                Id_category = model.Id_category,
                Name = model.Name,
                Priority = model.Priority,
                Description = model.Description,
                CategoryName = model.Category?.Name,
                TaskName = model.Task?.Select(o => o.Name).ToList(),
                TaskPriority = model.Task?.Select(o => o.Priority).ToList()
            };
        }
        public void Update(IDataToDoList model)
        {
            using (var context = new ToDoListEntities1())
            {
                var modelToUpdate = context.ToDoList
                    .SingleOrDefault(o => model.IdToDoList.Equals(o.IdToDoList));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Id_category = model.Id_category;
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Priority = model.Priority;
                    modelToUpdate.Description = model.Description;
                }

                context.SaveChanges();
            }
        }
		public bool Any(Func<ToDoList, bool> predicate)
		{
			bool result = false;

			using (var context = new ToDoListEntities1())
			{
				result = context.ToDoList.Any(predicate);
			}

			return result;
		}
	}
}
