﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Task_Entity_Fr.Models;
using DAL_Entity.Repository;
using DAL.Interface.Interface.DataModels;
using BLLs = BLL.Interface.Interface.DataModels;
using System;

namespace BLL.Task_Entity_Fr.Services
{
	public class CommentServices
	{
		private readonly CommentRepository _commentRepository;
		public CommentServices()
		{
			_commentRepository = new CommentRepository();
		}
		public bool Create(BLLs.IDataComment model)
		{
			return _commentRepository.Create(ToDataComment(model));
		}

		public bool Create(IEnumerable<BLLs.IDataComment> model)
		{
			return _commentRepository.Create(model.Select(ToDataComment));
		}

		public void Update(BLLs.IDataComment model)
		{
			bool exists = _commentRepository.Any(o => o.IdComment.Equals(model.IdComment));

			if (exists)
			{
				_commentRepository.Update(ToDataComment(model));
			}
			// TODO: Implement custom handling of errors
		}

		public void Delete(int id)
		{
			_commentRepository.Delete(id);
		}

		//public IEmployeeModel GetAllEmployees()
		//{

		//}

		private IDataComment ToDataComment(BLLs.IDataComment model)
		{
			return new CommentModel
			{
				Id_task = model.Id_task,
				Comment1 = model.Comment1,
				Date_creation = DateTime.Now
			};
		}
	}
}
