﻿using System;
using System.Collections.Generic;

namespace DAL.Interface.Interface
{
	public interface IRepository<TEntity, TDto>
    {
        bool Create(TDto model);
        bool Create(IEnumerable<TDto> model);
        void Update(TDto model);
        void Delete(int id);
        IEnumerable<TDto> Get(Func<TEntity, bool> predicate, int page, int pageSize);
     
    }
}
