﻿namespace DAL.Interface.Interface.DataModels
{
	public interface IDataUser
    {
         int IdUser { get;  }
         string Name { get;  }
         string Password { get;  }
    }
}
