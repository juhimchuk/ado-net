﻿namespace DAL.Interface.Interface.DataModels
{
	public interface IDataCategory
    {
         int IdCategory { get;  }
         int IdUser { get;  }
         string Name { get;  }
    }
}
