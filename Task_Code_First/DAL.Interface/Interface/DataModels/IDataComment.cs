﻿using System;

namespace DAL.Interface.Interface.DataModels
{
	public interface IDataComment
    {
         int IdComment { get;  }
         int IdTask { get;  }
         string Comment1 { get;  }
         DateTime DateCreation { get;  }
    }
}
