﻿using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.DTO
{
	public class CommentDto:IDataComment
    {
        public int IdComment { get; set; }
        public int IdTask { get; set; }
        public string Comment1 { get; set; }
        public System.DateTime DateCreation { get; set; }
        public string TaskName { get; set; }
    }
}
