﻿using System.Collections.Generic;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.DTO
{
	public class UserDto:IDataUser
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public List<string> CategoryName { get; set; }
    }
}
