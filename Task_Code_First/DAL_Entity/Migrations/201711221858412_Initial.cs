namespace DAL_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        IdCategory = c.Int(nullable: false, identity: true),
                        Id_user = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, fixedLength: true),
                    })
                .PrimaryKey(t => t.IdCategory)
                .ForeignKey("dbo.User", t => t.Id_user)
                .Index(t => t.Id_user);
            
            CreateTable(
                "dbo.ToDoList",
                c => new
                    {
                        IdToDoList = c.Int(nullable: false, identity: true),
                        Id_category = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, fixedLength: true),
                        Priority = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdToDoList)
                .ForeignKey("dbo.Category", t => t.Id_category)
                .Index(t => t.Id_category);
            
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        IdTask = c.Int(nullable: false, identity: true),
                        Id_todolist = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, fixedLength: true),
                        Description = c.String(nullable: false),
                        Priority = c.Int(nullable: false),
                        Status = c.String(nullable: false, maxLength: 50, fixedLength: true),
                        Date_creation = c.DateTime(nullable: false, storeType: "date"),
                        Date_finish = c.DateTime(nullable: false, storeType: "date"),
                        Reminders = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdTask)
                .ForeignKey("dbo.ToDoList", t => t.Id_todolist)
                .Index(t => t.Id_todolist);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        IdComment = c.Int(nullable: false, identity: true),
                        Id_task = c.Int(nullable: false),
                        Comment = c.String(nullable: false),
                        Date_creation = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.IdComment)
                .ForeignKey("dbo.Task", t => t.Id_task)
                .Index(t => t.Id_task);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        IdUser = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, fixedLength: true),
                        Password = c.String(nullable: false, maxLength: 50, fixedLength: true),
                    })
                .PrimaryKey(t => t.IdUser);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Category", "Id_user", "dbo.User");
            DropForeignKey("dbo.ToDoList", "Id_category", "dbo.Category");
            DropForeignKey("dbo.Task", "Id_todolist", "dbo.ToDoList");
            DropForeignKey("dbo.Comment", "Id_task", "dbo.Task");
            DropIndex("dbo.Comment", new[] { "Id_task" });
            DropIndex("dbo.Task", new[] { "Id_todolist" });
            DropIndex("dbo.ToDoList", new[] { "Id_category" });
            DropIndex("dbo.Category", new[] { "Id_user" });
            DropTable("dbo.User");
            DropTable("dbo.Comment");
            DropTable("dbo.Task");
            DropTable("dbo.ToDoList");
            DropTable("dbo.Category");
        }
    }
}
