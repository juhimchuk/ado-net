namespace DAL_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ToDoList", "Id_category", "dbo.Category");
            DropForeignKey("dbo.Category", "Id_user", "dbo.User");
            DropForeignKey("dbo.Task", "Id_todolist", "dbo.ToDoList");
            DropForeignKey("dbo.Comment", "Id_task", "dbo.Task");
            RenameColumn(table: "dbo.ToDoList", name: "Id_category", newName: "IdCategory");
            RenameColumn(table: "dbo.Category", name: "Id_user", newName: "IdUser");
            RenameColumn(table: "dbo.Task", name: "Id_todolist", newName: "IdTodolist");
            RenameColumn(table: "dbo.Comment", name: "Id_task", newName: "IdTask");
            RenameIndex(table: "dbo.Category", name: "IX_Id_user", newName: "IX_IdUser");
            RenameIndex(table: "dbo.ToDoList", name: "IX_Id_category", newName: "IX_IdCategory");
            RenameIndex(table: "dbo.Task", name: "IX_Id_todolist", newName: "IX_IdTodolist");
            RenameIndex(table: "dbo.Comment", name: "IX_Id_task", newName: "IX_IdTask");
            AddColumn("dbo.Task", "DateCreation", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Task", "DateFinish", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Comment", "DateCreation", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("dbo.Category", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.ToDoList", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Task", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Task", "Status", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.User", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.User", "Password", c => c.String(nullable: false, maxLength: 50));
            AddForeignKey("dbo.ToDoList", "IdCategory", "dbo.Category", "IdCategory", cascadeDelete: true);
            AddForeignKey("dbo.Category", "IdUser", "dbo.User", "IdUser", cascadeDelete: true);
            AddForeignKey("dbo.Task", "IdTodolist", "dbo.ToDoList", "IdToDoList", cascadeDelete: true);
            AddForeignKey("dbo.Comment", "IdTask", "dbo.Task", "IdTask", cascadeDelete: true);
            DropColumn("dbo.Task", "Date_creation");
            DropColumn("dbo.Task", "Date_finish");
            DropColumn("dbo.Comment", "Date_creation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comment", "Date_creation", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Task", "Date_finish", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.Task", "Date_creation", c => c.DateTime(nullable: false, storeType: "date"));
            DropForeignKey("dbo.Comment", "IdTask", "dbo.Task");
            DropForeignKey("dbo.Task", "IdTodolist", "dbo.ToDoList");
            DropForeignKey("dbo.Category", "IdUser", "dbo.User");
            DropForeignKey("dbo.ToDoList", "IdCategory", "dbo.Category");
            AlterColumn("dbo.User", "Password", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.User", "Name", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Task", "Status", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Task", "Name", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.ToDoList", "Name", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Category", "Name", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            DropColumn("dbo.Comment", "DateCreation");
            DropColumn("dbo.Task", "DateFinish");
            DropColumn("dbo.Task", "DateCreation");
            RenameIndex(table: "dbo.Comment", name: "IX_IdTask", newName: "IX_Id_task");
            RenameIndex(table: "dbo.Task", name: "IX_IdTodolist", newName: "IX_Id_todolist");
            RenameIndex(table: "dbo.ToDoList", name: "IX_IdCategory", newName: "IX_Id_category");
            RenameIndex(table: "dbo.Category", name: "IX_IdUser", newName: "IX_Id_user");
            RenameColumn(table: "dbo.Comment", name: "IdTask", newName: "Id_task");
            RenameColumn(table: "dbo.Task", name: "IdTodolist", newName: "Id_todolist");
            RenameColumn(table: "dbo.Category", name: "IdUser", newName: "Id_user");
            RenameColumn(table: "dbo.ToDoList", name: "IdCategory", newName: "Id_category");
            AddForeignKey("dbo.Comment", "Id_task", "dbo.Task", "IdTask");
            AddForeignKey("dbo.Task", "Id_todolist", "dbo.ToDoList", "IdToDoList");
            AddForeignKey("dbo.Category", "Id_user", "dbo.User", "IdUser");
            AddForeignKey("dbo.ToDoList", "Id_category", "dbo.Category", "IdCategory");
        }
    }
}
