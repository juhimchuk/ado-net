namespace DAL_Entity.Model
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("Comment")]
    public class Comment
    {
        [Key]
        public int IdComment { get; set; }

        public int IdTask { get; set; }

        [Column("Comment")]
        [Required]
        public string Comment1 { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateCreation { get; set; }
	    [ForeignKey("IdTask")]
		public virtual Task Task { get; set; }
    }
}
