namespace DAL_Entity.Model
{
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("ToDoList")]
    public  class ToDoList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
      
        [Key]
        public int IdToDoList { get; set; }

        public int IdCategory { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int Priority { get; set; }

        [Required]
        public string Description { get; set; }
	    [ForeignKey("IdCategory")]
		public virtual Category Category { get; set; }
	    
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Task> Task { get; set; }
    }
}
