﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL_Entity.Model
{
    public  class ToDoListEntities : DbContext
    {
        public ToDoListEntities()
            : base("name=ToDoListEntities")
        {
        }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<ToDoList> ToDoList { get; set; }
       
    }
}
