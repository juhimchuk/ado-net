﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class ToDoListRepository : IRepository<ToDoList, IDataToDoList>
    {
        private ToDoList DtoToEntity(IDataToDoList model)
        {
            return new ToDoList
            {
                IdCategory = model.IdCategory,
                Name = model.Name,
                Priority = model.Priority,
                Description = model.Description
            };
        }
        public bool Create(IDataToDoList model)
        {
            var todolist = DtoToEntity(model);
			bool result;
			using (var context = new ToDoListEntities())
            {
                context.ToDoList.Add(todolist);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataToDoList> model)
        {
            var todolistEntities = model.Select(DtoToEntity);

            bool result;

            using (var context = new ToDoListEntities())
            {
                context.ToDoList.AddRange(todolistEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities())
            {
                var todolist = context.ToDoList
                    .SingleOrDefault(o => o.IdToDoList.Equals(id));

                if (todolist != null)
                {
                    context.ToDoList.Remove(todolist);
					context.SaveChanges();
				}

               
            }
        }

        public IEnumerable<IDataToDoList> Get(Func<ToDoList, bool> predicate, int page=-1, int pageSize=-1)
        {
            var result = new List<IDataToDoList>();
			if (page <= 0)
			{
				page = 1;
				pageSize = 5;
			}
			var skipRows = (page - 1) * pageSize;
            using (var context = new ToDoListEntities())
            {
				 result = context.ToDoList
                    .Include(nameof(Category))
                    .Include(nameof(Task))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).Skip(skipRows).Take(pageSize).ToList();
            }
            
            return result;
        }
   
        private static IDataToDoList EntityToDto(ToDoList model)
        {
            return new ToDoListDto
            {
                IdToDoList = model.IdToDoList,
                IdCategory = model.IdCategory,
                Name = model.Name,
                Priority = model.Priority,
                Description = model.Description,
                CategoryName = model.Category?.Name,
                TaskName = model.Task?.Select(o => o.Name).ToList(),
                TaskPriority = model.Task?.Select(o => o.Priority).ToList()
            };
        }
        public void Update(IDataToDoList model)
        {
            using (var context = new ToDoListEntities())
            {
                var modelToUpdate = context.ToDoList
                    .SingleOrDefault(o => model.IdToDoList.Equals(o.IdToDoList));
                if (modelToUpdate != null)
                {
                    modelToUpdate.IdCategory = model.IdCategory;
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Priority = model.Priority;
                    modelToUpdate.Description = model.Description;
                }

                context.SaveChanges();
            }
        }
		public bool Any(Func<ToDoList, bool> predicate)
		{
			bool result;

			using (var context = new ToDoListEntities())
			{
				result = context.ToDoList.Any(predicate);
			}

			return result;
		}
	}
}
