﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
	public class TaskRepository : IRepository<Task, IDataTask>
    {
        private Task DtoToEntity(IDataTask model)
        {
            return new Task
            {
                IdTodolist=model.IdTodolist,
                Name = model.Name,
                Description=model.Description,
                Priority = model.Priority,
                Status=model.Status,
                DateCreation=model.DateCreation,
                DateFinish=model.DateFinish,
                Reminders=model.Reminders
            };
        }
        public bool Create(IDataTask model)
        {
            var task = DtoToEntity(model);
			bool result ;
			using (var context = new ToDoListEntities())
            {
                context.Task.Add(task);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataTask> model)
        {
            var taskEntities = model.Select(DtoToEntity);

            bool result;

            using (var context = new ToDoListEntities())
            {
                context.Task.AddRange(taskEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities())
            {
                var task = context.Task
                    .SingleOrDefault(o => o.IdTask.Equals(id));

                if (task != null)
                {
                    context.Task.Remove(task);
					context.SaveChanges();
				}

              
            }
        }

        public IEnumerable<IDataTask> Get(Func<Task, bool> predicate, int page=-1, int pageSize=-1)
        {
            var result = new List<IDataTask>();
			if (page <= 0)
			{
				page = 1;
				pageSize = 5;
			}
			int skipRows = (page - 1) * pageSize;
            using (var context = new ToDoListEntities())
            {
                result = context.Task
                    .Include(nameof(ToDoList))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).Skip(skipRows).Take(pageSize).ToList();
            }
           
            return result;
        }
        private IDataTask EntityToDto(Task model)
        {
            return new TaskDto
            {
                IdTask = model.IdTask,
                IdTodolist = model.IdTodolist,
                Name = model.Name,
                Description = model.Description,
                Priority = model.Priority,
                Status = model.Status,
                DateCreation = model.DateCreation,
                DateFinish = model.DateFinish,
                Reminders = model.Reminders,
                ListName = model.ToDoList?.Name
            };
        }
       
        public void Update(IDataTask model)
        {
            using (var context = new ToDoListEntities())
            {
                var modelToUpdate = context.Task
                    .SingleOrDefault(o => model.IdTask.Equals(o.IdTask));
                if (modelToUpdate != null)
                {
                    modelToUpdate.IdTodolist = model.IdTodolist;
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Description = model.Description;
                    modelToUpdate.Priority = model.Priority;
                    modelToUpdate.Status = model.Status;
                    modelToUpdate.DateFinish = model.DateFinish;
                    modelToUpdate.Reminders = model.Reminders;
					context.SaveChanges();
				}

               
            }

        }
		public bool Any(Func<Task, bool> predicate)
		{
			bool result;

			using (var context = new ToDoListEntities())
			{
				result = context.Task.Any(predicate);
			}

			return result;
		}
	}
}
