﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
	public class CommentRepository : IRepository<Comment, IDataComment>
    {
        private Comment DtoToEntity(IDataComment model)
        {
            return new Comment
            {
                IdTask=model.IdTask,
                Comment1=model.Comment1,
                DateCreation=model.DateCreation
            };
        }
        public bool Create(IDataComment model)
        {
            var comment = DtoToEntity(model);
			bool result;
			using (var context = new ToDoListEntities())
            {
                context.Comment.Add(comment);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataComment> model)
        {
            var commentEntities = model.Select(DtoToEntity);

            bool result;

            using (var context = new ToDoListEntities())
            {
                context.Comment.AddRange(commentEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities())
            {
                var comment = context.Comment
                    .SingleOrDefault(o => o.IdComment.Equals(id));

                if (comment != null)
                {
                    context.Comment.Remove(comment);
					context.SaveChanges();
				}

               
            }
        }

        
        
    public IEnumerable<IDataComment> Get(Func<Comment, bool> predicate, int page=-1, int pageSize=-1)
    {
            var result = new List<IDataComment>();
			if (page <= 0)
			{
				page = 1;
				pageSize = 5;
			}
			int skipRows = (page - 1) * pageSize;

                using (var context = new ToDoListEntities())
                {
                    result = context.Comment
                        .Where(predicate)
                        .Select(o => EntityToDto(o)).Skip(skipRows).Take(pageSize).ToList();
                }
           
            return result;
        }
        private IDataComment EntityToDto(Comment model)
        {
            return new CommentDto
            {
                IdComment= model.IdComment,
                IdTask = model.IdTask,
                DateCreation = model.DateCreation,
                Comment1 = model.Comment1
            };
        }

        public void Update(IDataComment model)
        {
            using (var context = new ToDoListEntities())
            {
                var modelToUpdate = context.Comment
                    .SingleOrDefault(o => model.IdComment.Equals(o.IdComment));
                if (modelToUpdate != null)
                {
                    modelToUpdate.IdTask = model.IdTask;
                    modelToUpdate.Comment1 = model.Comment1;
					context.SaveChanges();
				}

              
            }
        }
     
        public bool Any(Func<Comment, bool> predicate)
		{
			bool result ;

			using (var context = new ToDoListEntities())
			{
				result = context.Comment.Any(predicate);
			}

			return result;
		}
	}
}
