﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
    public class UserRepository : IRepository<User, IDataUser>
    {
        private User ToEntity(IDataUser model)
        {
            return new User
            {
                Name = model.Name,
                Password = model.Password,
              
            };
        }
        public bool Create(IDataUser model)
        {
            var user = ToEntity(model);
			bool result ;
			using (var context = new ToDoListEntities())
            {
                context.User.Add(user);

				result = context.SaveChanges() > 0;
			}

			return result;
		}

        public bool Create(IEnumerable<IDataUser> model)
        {
            var userEntities = model.Select(ToEntity);

            bool result ;

            using (var context = new ToDoListEntities())
            {
                context.User.AddRange(userEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities())
            {
                var user = context.User
                    .SingleOrDefault(o => o.IdUser.Equals(id));

                if (user != null)
                {
                    context.User.Remove(user);
	                context.SaveChanges();
				}

                
            }
        }

        public IEnumerable<IDataUser> Get(Func<User, bool> predicate, int page=-1, int pageSize=-1)
        {
            var result = new List<IDataUser>();
			if (page <= 0)
			{
				page = 1;
				pageSize = 5;
			}
			int skipRows = (page - 1) * pageSize;
            using (var context = new ToDoListEntities())
            {
                result = context.User
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).Skip(skipRows).Take(pageSize).ToList();
            }
          
            return result;
        }
      
        private IDataUser EntityToDto(User model)
        {
            return new UserDto
            {
                IdUser=model.IdUser,
                Name=model.Name,
                Password=model.Password
            };
        }
        public void Update(IDataUser model)
        {
            using (var context = new ToDoListEntities())
            {
                var modelToUpdate = context.User
                    .SingleOrDefault(o => model.IdUser.Equals(o.IdUser));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.Password = model.Password;
	                context.SaveChanges();
				}

               
            }

        }
        public bool Any(Func<User, bool> predicate)
        {
            bool result ;

            using (var context = new ToDoListEntities())
            {
                result = context.User.Any(predicate);
            }

            return result;
        }
    }
}
