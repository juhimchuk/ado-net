﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL_Entity.DTO;
using DAL_Entity.Model;
using DAL.Interface.Interface;
using DAL.Interface.Interface.DataModels;

namespace DAL_Entity.Repository
{
	public class CategoryRepository : IRepository<Category, IDataCategory>
    {
        private Category DtoToEntity(IDataCategory model)
        {
			return new Category
			{
				
                Name = model.Name,
                IdUser = model.IdUser               
            };
        }
        public bool Create(IDataCategory model)
        {
            var category = DtoToEntity(model);
			bool result;

			using (var context = new ToDoListEntities())
            {
                context.Category.Add(category);

				result = context.SaveChanges() > 0;
			}

            return result;
        }

        public bool Create(IEnumerable<IDataCategory> model)
        {
            var categoryEntities = model.Select(DtoToEntity);

            bool result;

            using (var context = new ToDoListEntities())
            {
                context.Category.AddRange(categoryEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListEntities())
            {
                var category = context.Category
                    .SingleOrDefault(o => o.IdCategory.Equals(id));

                if (category != null)
                {
                    context.Category.Remove(category);
					context.SaveChanges();
				}

                
            }
        }
       
        public IEnumerable<IDataCategory> Get(Func<Category, bool> predicate, int page=-1, int pageSize=-1)
        {
            var result = new List<IDataCategory>();
			if (page <= 0) {
				page = 1;
				pageSize = 5;
			}
            int skipRows = (page - 1) * pageSize;
            using (var context = new ToDoListEntities())
            {
                result = context.Category
                    .Include(nameof(User))
                    .Where(predicate)
                    .Select(o => EntityToDto(o)).Skip(skipRows).Take(pageSize).ToList();
            }
           
            return result;
        }
        private IDataCategory EntityToDto(Category model)
        {
            return new CategoryDto
            {
                Name = model.Name,
                IdCategory = model.IdCategory,
                IdUser = model.IdUser,
                UserName = model.User?.Name
            };
        }
        public void Update(IDataCategory model)
        {
            using (var context = new ToDoListEntities())
            {
                var modelToUpdate = context.Category
                    .SingleOrDefault(o => model.IdCategory.Equals(o.IdCategory));
                if (modelToUpdate != null)
                {
                    modelToUpdate.Name = model.Name;
                    modelToUpdate.IdUser = model.IdUser;
					context.SaveChanges();
				}

               
            }
        }
		public bool Any(Func<Category, bool> predicate)
		{
			bool result;

			using (var context = new ToDoListEntities())
			{
				result = context.Category.Any(predicate);
			}

			return result;
		}
	}
}
