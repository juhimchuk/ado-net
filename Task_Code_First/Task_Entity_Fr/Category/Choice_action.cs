﻿using System;

namespace Task_Entity_Fr.Category
{
	public class ChoiceAction
    {
        public void Choice(int iduser)
        {
            Console.WriteLine("Choice of action.");
            Console.WriteLine(@"1 - View all category.
2 - Add new category.
3 - Delete category.
4 - Update category.
5 - Change name category.
back - back to login.");

            string choice;
	        var helper = new ConsoleLogHelper();
			do
            {
                Console.Write("Enter action - ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":

	                    helper.ViewCat(iduser);
                        break;
                    case "2":

	                    helper.AddCategories(iduser);
                        break;
                    case "3":
                        
	                    helper.DeleteCat(iduser);
                        break;
                    case "4":
                        var update = new UpdateCategory();
                        update.UpdateCat(iduser);
                        break;
                    case "5":
                        
	                    helper.Change(iduser);
                        break;
                    case "back":
                        var back = new UserLogins();
                        back.UserLogin();
                        break;
                    default:
                        Console.WriteLine("Select the correct action.");
                        choice = "repeat";
                        break;
                }
            }
            while (choice == "repeat");
        }
    }
}
