﻿using System;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;


namespace Task_Entity_Fr.Category
{
	class ConsoleLogHelper
	{
		public void AddCategories(int iduser)
		{
			bool control;
			string catName;
			do
			{
				Console.Write("Enter category name - ");
				catName = Console.ReadLine();
				control = String.IsNullOrEmpty(catName);
			}
			while (control);
			var viewModel = new CategoryViewModel
			{
				Name = catName,
				IdUser = iduser
			};
			var service = new CategoryServices();

			var cat = service.Get(viewModel);
			int count = 0;
			foreach (var dir in cat)
			{
				if (catName == dir.Name.Trim() && iduser == dir.IdUser)
				{
					count++;
				}
			}
			if (count == 0)
			{
				service.Create(viewModel);
			}
			else
			{
				Console.WriteLine("Enter another category name ; ");
			}
			Console.WriteLine();
			var choice = new ChoiceAction();
			choice.Choice(iduser);
		}
		public void Change(int iduser)
		{
			int count = 0;

			var viewModel = new CategoryViewModel
			{
				IdUser = iduser
			};

			var service = new CategoryServices();
			var cat = service.Get(viewModel);
			string catName;
			do
			{
				int idcategory = -1;
				Console.WriteLine();
				Console.Write("Enter category name - ");
				catName = Console.ReadLine();
				Console.Write("Enter new category name - ");
				string newCatName = Console.ReadLine();
				foreach (var dir in cat)
				{
					if (catName == dir.Name.Trim())
					{
						idcategory = dir.IdCategory;
						count++;
					}
				}
				if (count != 0)
				{
					if (catName != newCatName)
					{
						viewModel = new CategoryViewModel
						{
							IdUser = iduser,
							IdCategory = idcategory,
							Name = newCatName
						};
						service.Update(viewModel);
						catName = "repeat";
					}
					else
					{
						Console.WriteLine("Enter a name for the category other than the existing one.");
					}
				}
				else
				{
					Console.WriteLine("Enter the name of the category that exists.");
				}

			}
			while (catName != "repeat");
			Console.WriteLine();
			var choice = new ChoiceAction();
			choice.Choice(iduser);
		}
		public void DeleteCat(int iduser)
		{
			int count = 0;
			do
			{
				var viewModel = new CategoryViewModel
				{
					IdUser = iduser
				};
				var service = new CategoryServices();
				var cat = service.Get(viewModel);
				Console.Write("Enter category name - ");
				string catName = Console.ReadLine();
				foreach (var dir in cat)
				{
					if (catName == dir.Name.Trim())
					{
						service.Delete(dir.IdCategory);
						count++;
					}
				}
			}
			while (count == 0);
			Console.WriteLine();
			var choice = new ChoiceAction();
			choice.Choice(iduser);
		}
		public void ViewCat(int iduser)
		{
			var viewModel = new CategoryViewModel
			{
				IdUser = iduser
			};

			var service = new CategoryServices();
			string page;
			bool control;
			do
			{
				Console.Write("Enter number page - ");
				page = Console.ReadLine();
				int number;
				control = Int32.TryParse(page, out number);
			}
			while (!control);
			int pages = Convert.ToInt32(page);
			string pageSize;
			do
			{
				Console.Write("Enter size page - ");
				pageSize = Console.ReadLine();
				int number;
				control = Int32.TryParse(pageSize, out number);
			}
			while (!control);
			int pagesizes = Convert.ToInt32(pageSize);
			var cat = service.Get(viewModel, pages, pagesizes);
			Console.WriteLine("Category:");
			foreach (var dir in cat)
			{
				Console.WriteLine(dir.Name);
			}
			Console.WriteLine();
			var choice = new ChoiceAction();
			choice.Choice(iduser);
		}
	}
}
