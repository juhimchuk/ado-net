﻿using System;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.ToDoList;

namespace Task_Entity_Fr.Category
{
	public class UpdateCategory
    {
        public void UpdateCat(int iduser)
        {
            var update = new UpdateCategory();
          
            Console.WriteLine("Choice of action.");
            Console.WriteLine(@"1. View all todolist.
2. Add new todolist.
3. Delete todolist.
back.");
            var choice = Console.ReadLine();
			var helper = new ToDoListHelper();
            do
            {
                switch (choice)
                {
                    case "1":
	                    var idcat = update.Choice_cat(iduser);
	                    helper.ViewTodolist( idcat, iduser);
                        break;
                    case "2":
                        idcat = update.Choice_cat(iduser);
	                    helper.AddTodolist(idcat, iduser);
                        break;
                    case "3":
                        idcat = update.Choice_cat(iduser);
	                    helper.DeleteTodolist(iduser, idcat);
                        break;
                    case "back":
                        var choic = new ChoiceAction();
                        choic.Choice(iduser);
                        break;
                    default:
                        Console.WriteLine("Select the correct action.");
                        choice = "repeat";
                        break;
                }
            }
            while (choice == "repeat");
        }
        public int Choice_cat(int iduser)
        {
            int count = 0;
            int idcat = -1;
            string catName;
            do
            {
                Console.WriteLine();
                Console.Write("Enter category name - ");
                catName = Console.ReadLine();
                var viewModel = new CategoryViewModel
                {
                    IdUser = iduser
                };
                
                var service = new CategoryServices();
                var cat = service.Get(viewModel);
                foreach (var dir in cat)
                {
                    if (catName == dir.Name.Trim())
                    {
                        idcat = dir.IdCategory;
                        count++;
                    }
                }
               
                if (count == 0)
                {

                    Console.WriteLine("Enter the name of the category that exists.");
                }
            }
            while (count == 0);
            Console.WriteLine();

            return idcat;
        }
    }
}
