﻿using System.Collections.Generic;
using BLL.Interface.Interface.DataModels;

namespace Task_Entity_Fr.ViewModel
{
	public class TaskViewModel:IDataTask
	{
		public int IdTask { get; set; }
		public int IdTodolist { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int Priority { get; set; }
		public string Status { get; set; }
		public System.DateTime DateCreation { get; set; }
		public System.DateTime DateFinish { get; set; }
		public int Reminders { get; set; }
		public string ListName { get; set; }
		public List<string> Comment { get; set; }
	}
}
