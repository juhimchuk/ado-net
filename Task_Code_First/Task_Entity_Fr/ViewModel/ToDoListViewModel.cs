﻿using System.Collections.Generic;
using BLL.Interface.Interface.DataModels;

namespace Task_Entity_Fr.ViewModel
{
	public class ToDoListViewModel:IDataToDoList
	{
		public int IdToDoList { get; set; }
		public int IdCategory { get; set; }
		public string Name { get; set; }
		public int Priority { get; set; }
		public string Description { get; set; }
		public string CategoryName { get; set; }
		public List<string> TaskName { get; set; }
		public List<int> TaskPriority { get; set; }
	}
}
