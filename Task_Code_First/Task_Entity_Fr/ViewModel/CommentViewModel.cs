﻿using BLL.Interface.Interface.DataModels;

namespace Task_Entity_Fr.ViewModel
{
	public class CommentViewModel:IDataComment
	{
		public int IdComment { get; set; }
		public int IdTask { get; set; }
		public string Comment { get; set; }
		public System.DateTime DateCreation { get; set; }
		public string TaskName { get; set; }
	}
}
