﻿using System;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.Category;

namespace Task_Entity_Fr
{
	public class UserLogins
    {
        public void UserLogin()
        {
            string login, password;
            do
            {
                Console.Write("Enter login - ");
                login = Console.ReadLine();
                if (login != "back")
                {

                    Console.Write("Enter password - ");
                    password = Console.ReadLine();
                    var viewModel = new UserViewModel
                    {
                        Name = login,
                        Password = password
                    };

                    var service = new UserServices();
                    var user = service.Get(viewModel);
	                int iduser=-1;
	                foreach (var u in user)
	                {
		                iduser= u.IdUser;
	                }
					if (iduser >= 0)
                    {
                     
                     var choice = new ChoiceAction();
                     choice.Choice(iduser);
                    }
                    else
                    {

                        Console.WriteLine("Incorrect login or password.");
                        login = "repeat";
                    }


                }
            }
            while (login == "repeat");

        }
    }
}
