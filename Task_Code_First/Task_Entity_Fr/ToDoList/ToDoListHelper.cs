﻿using System;
using BLL.Task_Entity_Fr.Services;
using Task_Entity_Fr.ViewModel;
using Task_Entity_Fr.Category;

namespace Task_Entity_Fr.ToDoList
{
	class ToDoListHelper
	{
		public void AddTodolist(int idcat, int iduser)
		{
			string todoName, descript, priority, save;
			bool control;
			do
			{
				Console.Write("Enter todolist name - ");
				todoName = Console.ReadLine();
				control = String.IsNullOrEmpty(todoName);
			}
			while (control);
			do
			{
				Console.Write("Enter description - ");
				descript = Console.ReadLine();
				control = String.IsNullOrEmpty(descript);
			}
			while (control);
			do
			{
				Console.Write("Enter priority - ");
				priority = Console.ReadLine();
				int number;
				control = Int32.TryParse(priority, out number);
			}
			while (!control);
			int prior = Convert.ToInt32(priority);


			do
			{

				var viewModel = new ToDoListViewModel
				{
					IdCategory = idcat,
					Name = todoName,
					Priority = prior,
					Description = descript
				};
				var service = new ToDoListServices();
				var cat = service.Get(viewModel);
				int count = 0;
				foreach (var dir in cat)
				{
					if (todoName == dir.Name.Trim() && idcat == dir.IdCategory)
					{
						count++;
					}
				}
				if (count == 0)
				{
					service.Create(viewModel);
					save = "ok";
				}
				else
				{
					Console.WriteLine("Enter another todolist name ; ");
					save = "";
				}
				Console.WriteLine();
			}
			while (save == "");
			var update = new UpdateCategory();
			update.UpdateCat(iduser);
		}
		public void DeleteTodolist(int iduser, int idcat)
		{
			int count = 0;
			do
			{

				var viewModel = new ToDoListViewModel
				{
					IdCategory = idcat
				};
				var service = new ToDoListServices();
				var cat = service.Get(viewModel);
				Console.Write("Enter todolist name - ");
				string todoName = Console.ReadLine();
				foreach (var dir in cat)
				{
					if (todoName == dir.Name.Trim())
					{
						service.Delete(dir.IdToDoList);
						count++;
					}
				}
			}
			while (count == 0);

			Console.WriteLine();
			var update = new UpdateCategory();
			update.UpdateCat(iduser);
		}
		public void ViewTodolist(int idcat, int iduser)
		{
			var viewModel = new ToDoListViewModel
			{
				IdCategory = idcat
			};

			var service = new ToDoListServices();
			string page;
			bool control;
			do
			{
				Console.Write("Enter number page - ");
				page = Console.ReadLine();
				int number;
				control = Int32.TryParse(page, out number);
			}
			while (!control);
			int pages = Convert.ToInt32(page);
			string pageSize;
			do
			{
				Console.Write("Enter size page - ");
				pageSize = Console.ReadLine();
				int number;
				control = Int32.TryParse(pageSize, out number);
			}
			while (!control);
			int pagesizes = Convert.ToInt32(pageSize);
			var cat = service.Get(viewModel, pages, pagesizes);
			Console.WriteLine("ToDoList:");
			foreach (var dir in cat)
			{
				Console.WriteLine(dir.Name + " " + dir.Priority);
			}
			Console.WriteLine();

			var update = new UpdateCategory();
			update.UpdateCat(iduser);
		}
	}
}
