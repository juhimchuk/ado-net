﻿using System;

namespace Task_Entity_Fr
{
	class Program
    {
        static void Main(string[] args)
        {
            string choice;
            do
            {
                Console.WriteLine("Enter login for enter or logup for registration or exit for exit");
                Console.Write("Enter action - ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "login":
                        var login = new UserLogins();
                        login.UserLogin();
                        break;
                    case "logup":
                        var logup = new UserLogout();
                        logup.UserLogOut();
                        break;
                }
            }
            while (choice != "exit");
        
        }
    }
}
