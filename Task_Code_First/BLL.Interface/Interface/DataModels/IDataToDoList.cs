﻿namespace BLL.Interface.Interface.DataModels
{
	public interface IDataToDoList
    {
         int IdToDoList { get;  }
         int IdCategory { get;  }
         string Name { get;  }
         int Priority { get;  }
         string Description { get;  }
    }
}
