﻿using System;

namespace BLL.Interface.Interface.DataModels
{
	public interface IDataTask
    {
         int IdTask { get; }
         int IdTodolist { get;  }
         string Name { get;  }
         string Description { get;  }
         int Priority { get;  }
         string Status { get;  }
         DateTime DateCreation { get;  }
         DateTime DateFinish { get;  }
         int Reminders { get;  }
    }
}
