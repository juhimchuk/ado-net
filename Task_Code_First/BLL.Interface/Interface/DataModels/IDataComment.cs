﻿using System;

namespace BLL.Interface.Interface.DataModels
{
	public interface IDataComment
    {
         int IdComment { get;  }
         int IdTask { get;  }
         string Comment { get;  }
         DateTime DateCreation { get;  }
    }
}
