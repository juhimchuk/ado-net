﻿using System.Collections.Generic;
using DAL.Interface.Interface.DataModels;

namespace BLL.Task_Entity_Fr.Models
{
	class ToDoListModel:IDataToDoList
	{
		public int IdToDoList { get; set; }
		public int IdCategory { get; set; }
		public string Name { get; set; }
		public int Priority { get; set; }
		public string Description { get; set; }
		public string CategoryName { get; set; }
		public List<string> TaskName { get; set; }
		public List<int> TaskPriority { get; set; }
	}
}
