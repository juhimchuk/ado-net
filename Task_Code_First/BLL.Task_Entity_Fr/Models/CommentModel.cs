﻿using DAL.Interface.Interface.DataModels;

namespace BLL.Task_Entity_Fr.Models
{
	public class CommentModel:IDataComment
	{
		public int IdComment { get; set; }
		public int IdTask { get; set; }
		public string Comment1 { get; set; }
		public System.DateTime DateCreation { get; set; }
		public string TaskName { get; set; }
	}
}
