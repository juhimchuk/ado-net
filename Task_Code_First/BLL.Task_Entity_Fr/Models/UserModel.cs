﻿using System.Collections.Generic;
using DAL.Interface.Interface.DataModels;

namespace BLL.Task_Entity_Fr.Models
{
	public class UserModel:IDataUser
	{
		public int IdUser { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
		public List<string> CategoryName { get; set; }
	}
}
