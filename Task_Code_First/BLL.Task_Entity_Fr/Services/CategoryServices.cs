﻿using System.Collections.Generic;
using System.Linq;
using BLL.Task_Entity_Fr.Models;
using DAL_Entity.Repository;
using DAL.Interface.Interface.DataModels;
using BLLs = BLL.Interface.Interface.DataModels;

namespace BLL.Task_Entity_Fr.Services
{
	public class CategoryServices
	{
		private readonly CategoryRepository _categoryRepository;
		public CategoryServices()
		{
			_categoryRepository = new CategoryRepository();
		}
		public bool Create(BLLs.IDataCategory model)
		{
			return _categoryRepository.Create(ToDataCategory(model));
		}

		public bool Create(IEnumerable<BLLs.IDataCategory> model)
		{
			return _categoryRepository.Create(model.Select(ToDataCategory));
		}

		public void Update(BLLs.IDataCategory model)
		{
			bool exists = _categoryRepository.Any(o => o.IdCategory.Equals(model.IdCategory));

			if (exists)
			{
				_categoryRepository.Update(ToDataCategory(model));
			}
			// TODO: Implement custom handling of errors
		}

		public void Delete(int id)
		{
			_categoryRepository.Delete(id);
		}

        //public IEmployeeModel GetAllEmployees()
        //{

        //}
        public IEnumerable<IDataCategory> Get(BLLs.IDataCategory model, int page=-1, int pageSize=-1)
        {
            var mod = ToDataCategory(model);
            return _categoryRepository.Get(o => Equals(o.IdUser, mod.IdUser),page,pageSize);

        }
        private IDataCategory ToDataCategory(BLLs.IDataCategory model)
		{
            if (model.IdCategory > 0)
                return new CategoryModel
			{
                IdCategory=model.IdCategory,
				Name = model.Name,
				IdUser = model.IdUser
			};
            return new CategoryModel
            {
                Name = model.Name,
                IdUser = model.IdUser
            };
        }
	}
}
