﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Task_Entity_Fr.Models;
using DAL_Entity.Repository;
using DAL.Interface.Interface.DataModels;
using BLLs = BLL.Interface.Interface.DataModels;


namespace BLL.Task_Entity_Fr.Services
{
	public class TaskServices
	{
		private readonly TaskRepository _taskRepository;
		public TaskServices()
		{
			_taskRepository = new TaskRepository();
		}
		public bool Create(BLLs.IDataTask model)
		{
			return _taskRepository.Create(ToDataTask(model));
		}

		public bool Create(IEnumerable<BLLs.IDataTask> model)
		{
			return _taskRepository.Create(model.Select(ToDataTask));
		}

		public void Update(BLLs.IDataTask model)
		{
			bool exists = _taskRepository.Any(o => o.IdTask.Equals(model.IdTask));

			if (exists)
			{
				_taskRepository.Update(ToDataTask(model));
			}
			// TODO: Implement custom handling of errors
		}

		public void Delete(int id)
		{
			_taskRepository.Delete(id);
		}

		//public IEmployeeModel GetAllEmployees()
		//{

		//}

		private IDataTask ToDataTask(BLLs.IDataTask model)
		{
			return new TaskModel
			{
				IdTodolist = model.IdTodolist,
				Name = model.Name,
				Description = model.Description,
				Priority = model.Priority,
				Status = model.Status,
				DateCreation = DateTime.Now,
				DateFinish = model.DateFinish,
				Reminders = model.Reminders
			};
		}
	}
}
