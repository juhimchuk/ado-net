﻿using System.Collections.Generic;
using System.Linq;
using BLL.Task_Entity_Fr.Models;
using DAL_Entity.Repository;
using DAL.Interface.Interface.DataModels;
using BLLs = BLL.Interface.Interface.DataModels;

namespace BLL.Task_Entity_Fr.Services
{
	public class UserServices
	{
		 private readonly UserRepository _userRepository;
        public UserServices()
        {
            _userRepository = new UserRepository();
        }
        public bool Create(BLLs.IDataUser model)
        {
           return _userRepository.Create(ToDataUser(model));
        }

        public bool Create(IEnumerable<BLLs.IDataUser> model)
        {
            return _userRepository.Create(model.Select(ToDataUser));
        }

        public void Update(BLLs.IDataUser model)
        {
            bool exists = _userRepository.Any(o => o.IdUser.Equals(model.IdUser));

            if (exists)
            {
               _userRepository.Update(ToDataUser(model));
            }
         
        }

        public void Delete(int id)
        {
            _userRepository.Delete(id);
        }

        public bool GetAll(BLLs.IDataUser model)
        {
            var mod= ToDataUser(model); 
            return _userRepository.Any(o => Equals(o.Name.Trim(),mod.Name) && Equals(o.Password.Trim(), mod.Password));
            
        }
        public IEnumerable<IDataUser> Get(BLLs.IDataUser model)
        {
            var mod = ToDataUser(model);
            return _userRepository.Get(o => Equals(o.Name.Trim(), mod.Name) && Equals(o.Password.Trim(), mod.Password));

        }

        private IDataUser ToDataUser(BLLs.IDataUser model)
        {
            return new UserModel
            {
				Name = model.Name,
				Password = model.Password
			};
        }
	}
}
