﻿using System.Collections.Generic;
using System.Linq;
using BLL.Task_Entity_Fr.Models;
using DAL_Entity.Repository;
using DAL.Interface.Interface.DataModels;
using BLLs = BLL.Interface.Interface.DataModels;

namespace BLL.Task_Entity_Fr.Services
{
	public class ToDoListServices
	{
		private readonly ToDoListRepository _todolistRepository;
		public ToDoListServices()
		{
			_todolistRepository = new ToDoListRepository();
		}
		public bool Create(BLLs.IDataToDoList model)
		{
			return _todolistRepository.Create(ToDataToDoList(model));
		}

		public bool Create(IEnumerable<BLLs.IDataToDoList> model)
		{
			return _todolistRepository.Create(model.Select(ToDataToDoList));
		}

		public void Update(BLLs.IDataToDoList model)
		{
			bool exists = _todolistRepository.Any(o => o.IdToDoList.Equals(model.IdToDoList));

			if (exists)
			{
				_todolistRepository.Update(ToDataToDoList(model));
			}
			// TODO: Implement custom handling of errors
		}

		public void Delete(int id)
		{
			_todolistRepository.Delete(id);
		}

        //public IEmployeeModel GetAllEmployees()
        //{

        //}
        public IEnumerable<IDataToDoList> Get(BLLs.IDataToDoList model, int page=-1, int pageSize=-1)
        {
            var mod = ToDataToDoList(model);
            return _todolistRepository.Get(o => Equals(o.IdCategory, mod.IdCategory),page,pageSize);

        }
        private IDataToDoList ToDataToDoList(BLLs.IDataToDoList model)
		{
            if (model.IdToDoList >= 0)
                return new ToDoListModel
			{
                
                IdToDoList=model.IdToDoList,
				IdCategory = model.IdCategory,
				Name = model.Name,
				Priority = model.Priority,
				Description = model.Description
			};
            return new ToDoListModel
            {
                
                IdCategory = model.IdCategory,
                Name = model.Name,
                Priority = model.Priority,
                Description = model.Description
            };
        }
	}
}
