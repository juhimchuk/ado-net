CREATE TABLE [dbo].[User](
	[id] [int] NOT NULL,
	[name] [nchar](50) NOT NULL,
	[password] [nchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


INSERT INTO [User](name,[password]) VALUES ( 'Mike', '123456');
INSERT INTO [User](name,[password]) VALUES ( 'Nike', 'qwertyui');
INSERT INTO [User](name,[password]) VALUES ( 'Jaroslav', 'viet152');
INSERT INTO [User](name,[password]) VALUES ( 'Xamarin', '123ert');
INSERT INTO [User](name,[password]) VALUES ( 'Prog', 'prog157');
INSERT INTO [User](name,[password]) VALUES ( 'zinia', 'zinia8345');