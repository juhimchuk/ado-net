﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserTable.Disconect
{
	public class Delete
	{
		public void delete(int id_delete)
		{
			var connectionDetails = ConfigurationManager.ConnectionStrings["DefaultConnection"];
			var providerName = connectionDetails.ProviderName;
			var connectionString = connectionDetails.ConnectionString;

			var queryString = "SELECT * FROM [User]";

			var dbFactory = DbProviderFactories.GetFactory(providerName);

			var adapter = dbFactory.CreateDataAdapter();

			var dataSet = new DataSet();

			using (var connection = dbFactory.CreateConnection())
			{

				// Create and configure a connection
				connection.ConnectionString = connectionString;

				// Create and configure a DbCommand
				var selectCommand = dbFactory.CreateCommand();
				selectCommand.CommandText = queryString;
				selectCommand.Connection = connection;

				// Create and configure a DbDataAdapter
				adapter.SelectCommand = selectCommand;

				// Parameters


				// Delete Command
				var delCmd = dbFactory.CreateCommand();

				delCmd.CommandText = "DELETE FROM [User] WHERE Id = @IdForDelete";
				
                var IdForDeleteParam = dbFactory.CreateParameter();
                IdForDeleteParam.DbType = DbType.Int32;
                IdForDeleteParam.ParameterName = "@IdForDelete";
                IdForDeleteParam.SourceColumn = "Id";
                IdForDeleteParam.DbType = DbType.Int32;
                delCmd.Parameters.Add(IdForDeleteParam);
				delCmd.Connection = connection;
				delCmd.CommandType = CommandType.Text;
				adapter.DeleteCommand = delCmd;

				adapter.Fill(dataSet);


				var users = dataSet.Tables[0];
				
                if (users.Rows.Count > id_delete)
                {
                    users.Rows[id_delete].Delete();
                }
                adapter.Update(users);

			}
		}
	}
}
