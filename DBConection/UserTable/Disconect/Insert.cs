﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserTable.Disconect
{
	public class Insert
	{
		public void insert(string name, string pass)
		{
			var connectionDetails = ConfigurationManager.ConnectionStrings["DefaultConnection"];
			var providerName = connectionDetails.ProviderName;
			var connectionString = connectionDetails.ConnectionString;

			var queryString = "SELECT * FROM [User]";

			var dbFactory = DbProviderFactories.GetFactory(providerName);

			var adapter = dbFactory.CreateDataAdapter();

			var dataSet = new DataSet();

			using (var connection = dbFactory.CreateConnection())
			{

				// Create and configure a connection
				connection.ConnectionString = connectionString;

				// Create and configure a DbCommand
				var selectCommand = dbFactory.CreateCommand();
				selectCommand.CommandText = queryString;
				selectCommand.Connection = connection;

				// Create and configure a DbDataAdapter
				adapter.SelectCommand = selectCommand;

				// Parameters


				var firstNameParam = dbFactory.CreateParameter();
				firstNameParam.DbType = DbType.String;
				firstNameParam.ParameterName = "@Name";
				firstNameParam.SourceColumn = "name";
				firstNameParam.DbType = DbType.String;

				var passwordParam = dbFactory.CreateParameter();
				passwordParam.DbType = DbType.String;
				passwordParam.ParameterName = "@Password";
				passwordParam.SourceColumn = "password";
				passwordParam.DbType = DbType.String;


				// Configure Insert Command
				var insCmd = dbFactory.CreateCommand();

				insCmd.CommandText = "INSERT INTO [User] ( [name], [password]) VALUES ( @Name, @Password)";

				insCmd.Parameters.Add(firstNameParam);
				insCmd.Parameters.Add(passwordParam);
				insCmd.Connection = connection;
				insCmd.CommandType = CommandType.Text;
				adapter.InsertCommand = insCmd;

				adapter.Fill(dataSet);
				var users = dataSet.Tables[0];
				users.Rows.Add(0,name,pass);
				adapter.Update(users);
			}
		}
	}
}
