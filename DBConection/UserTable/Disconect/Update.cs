﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserTable.Disconect
{
	public class Update
	{
		public void update(int id_user, string new_password)
		{
			// получаем строку подключения
			var connectionDetails = ConfigurationManager.ConnectionStrings["DefaultConnection"];
			var providerName = connectionDetails.ProviderName;
			var connectionString = connectionDetails.ConnectionString;

			var queryString = "SELECT * FROM [User]";

			var dbFactory = DbProviderFactories.GetFactory(providerName);

			var adapter = dbFactory.CreateDataAdapter();

			var dataSet = new DataSet();

			using (var connection = dbFactory.CreateConnection())
			{

				// Create and configure a connection
				connection.ConnectionString = connectionString;

				// Create and configure a DbCommand
				var selectCommand = dbFactory.CreateCommand();
				selectCommand.CommandText = queryString;
				selectCommand.Connection = connection;

				// Create and configure a DbDataAdapter
				adapter.SelectCommand = selectCommand;

				// Configure Update Command.
				var upCmd = dbFactory.CreateCommand();

				upCmd.CommandText = "UPDATE [User] SET password = @PasswordUpdate WHERE Id = @IdForUpdate";
				var IdForUpdateParam = dbFactory.CreateParameter();
				IdForUpdateParam.DbType = DbType.Int32;
				IdForUpdateParam.ParameterName = "@IdForUpdate";
				IdForUpdateParam.SourceColumn = "Id";
				IdForUpdateParam.DbType = DbType.Int32;
				upCmd.Parameters.Add(IdForUpdateParam);

				var firstNameUpdateParam = dbFactory.CreateParameter();
				firstNameUpdateParam.DbType = DbType.String;
				firstNameUpdateParam.ParameterName = "@PasswordUpdate";
				firstNameUpdateParam.SourceColumn = "password";
				firstNameUpdateParam.DbType = DbType.String;
				upCmd.Connection = connection;
				upCmd.CommandType = CommandType.Text;
				upCmd.Parameters.Add(firstNameUpdateParam);
				adapter.UpdateCommand = upCmd;

				adapter.Fill(dataSet);


				var users = dataSet.Tables[0];
				
					if (users.Rows.Count > id_user)
					{
						users.Rows[id_user]["password"] = new_password;
					}
				
				
				adapter.Update(users);
			}
		}
	}
}
