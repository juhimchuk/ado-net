﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserTable
{
    public class User
    {
		private string connectionString;
		private string strSql;

		private SqlCommand command { get; set; }
		public User(string str, string connectionStr)
		{
			strSql = str;
			connectionString = connectionStr;
			
		}

		public void select()
		{
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				try
				{
					connection.Open();
					SqlCommand command = new SqlCommand(strSql, connection);
					SqlDataReader myDataReader = command.ExecuteReader(CommandBehavior.Default);
					while (myDataReader.Read())
					{
						Console.WriteLine("-> Name - {0}, Password - {1}.",
							myDataReader["name"].ToString().Trim(),
							myDataReader["password"].ToString().Trim());
					}
				
				}
				catch (SqlException ex)
				{
					Console.WriteLine(ex.Message);
				}
				finally
				{
					connection.Close();
				}
			}
		}
	}
}
