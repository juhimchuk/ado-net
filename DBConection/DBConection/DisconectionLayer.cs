﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserTable.Disconect;

namespace DBConection
{
	class DisconectionLayer
	{
		public void Disconection()
		{
			string choose;
			do
			{
				Console.WriteLine("Choose action (exit,insert,update,delete)");
				choose = Console.ReadLine();
				string strSql = null;
				switch (choose)
				{
					case "update":
						Console.Write("Input id_user: ");
						int id_user =Int32.Parse(Console.ReadLine());
						Console.Write("Input new password: ");
						string password = Console.ReadLine();
						var upd = new Update();
						upd.update(id_user, password);
						break;
					case "insert":
						Console.Write("Input name: ");
						string name = Console.ReadLine();
						Console.Write("Input password: ");
						password = Console.ReadLine();
						var ins = new Insert();
						ins.insert(name, password);
						break;
					case "delete":
                        Console.Write("Input id_user: ");
                        int id_user_delete = Int32.Parse(Console.ReadLine());
                        var del = new Delete();
						del.delete(id_user_delete);
						break;
				}
			
			}
			while (choose != "exit");
		} 
	}
}
