﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserTable;


namespace DBConection
{
	class Program
	{
		static void Main(string[] args)
		{
            string choose;
            do {
                Console.WriteLine("Choose action (exit,disconectionLayer,conectionLayer)");
                
                choose = Console.ReadLine();
                switch (choose) {
                    case "disconectionLayer":
                        var disc = new DisconectionLayer();
                        disc.Disconection();
                        break;
                    case "conectionLayer":
                        var conLayer = new ConectionLayer();
                        conLayer.Conection();
                        break;

                }
            }
            while (choose != "exit");

            Console.Read();
		}
	}
}
