﻿using System;
using System.Collections.Generic;
using System.Configuration;
using UserTable;


namespace DBConection
{
	class ConectionLayer
	{
		public void Conection()
		{
			// получаем строку подключения
			string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
			string choose;

			do
			{
				Console.WriteLine("Choose action (exit,select,insert,update,delete)");
				choose = Console.ReadLine();
				string strSql = null;
				switch (choose)
				{
					case "select":
						strSql = "SELECT * FROM [User]";
						break;
					case "update":
						Console.Write("Input old name: ");
						string name = Console.ReadLine();
						Console.Write("Input new password: ");
						string password = Console.ReadLine();
						strSql = String.Format("UPDATE [User] SET [password]={1} WHERE [name]='{0}'", name, password);
						break;
					case "insert":
						Console.Write("Input name: ");
						name = Console.ReadLine();
						Console.Write("Input password: ");
						password = Console.ReadLine();
						strSql = String.Format("INSERT INTO [User] ([name], [password]) VALUES ('{0}','{1}')", name, password);
						break;
					case "delete":
						Console.Write("Input name: ");
						name = Console.ReadLine();
						strSql = String.Format("DELETE  FROM [User] WHERE [name]='{0}'", name);
						break;
				}
				if (strSql != null)
				{
					User sel = new User(strSql, connectionString);

					sel.select();
				}
			}
			while (choose != "exit");

			Console.Read();
		}
	}
}
